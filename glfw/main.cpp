#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <cmath>
#include <assert.h>

using namespace std;

const float TRANSLATION_STEP = 0.0005f;
float translation_x = 0;
float translation_y = 0;
int press_key = 0;
bool is_key_pressed = false;
int key_pressed = 0;

void drawObject(){
    glBegin(GL_QUADS);
    
    glColor3f(1.0f, 1.0f, 1.0f);
    glVertex3f(-0.1, -0.1, 0);
    
    glColor3f(1.0f, 1.0f, 0.0f);
    glVertex3f(-0.1, 0.1, 0);
    
    glColor3f(3.0f, 0.8f, 0.5f);
    glVertex3f(0.1, 0.1, 0);
    
    glColor3f(1.0f, 1.0f, 1.0f);
    glVertex3f(0.1, -0.1, 0);
    
    glEnd();

}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    switch(action){
        case GLFW_PRESS:
            is_key_pressed = true;
            key_pressed = key;
            break;
        case GLFW_RELEASE:
            is_key_pressed = false;
            break;
    }
}

void update_position_by_key(){
    switch(key_pressed){
        case GLFW_KEY_A:
            translation_x -= TRANSLATION_STEP;
            break;
        case GLFW_KEY_D:
            translation_x += TRANSLATION_STEP;
            break;
        case GLFW_KEY_W:
            translation_y += TRANSLATION_STEP;
            break;
        case GLFW_KEY_S:
            translation_y -= TRANSLATION_STEP;
            break;
        case GLFW_KEY_ESCAPE:
            exit(0);
            break;
    }
}

void update_position_randomly(){
    static float destination_x = 0.0f;
    static float destination_y = 0.0f;
    
    if (abs(translation_x - destination_x) < TRANSLATION_STEP &&
        abs(translation_y - destination_y) < TRANSLATION_STEP ){
        destination_x = (static_cast<float>(rand()) / (RAND_MAX/2)) - 1;
        destination_y = (static_cast<float>(rand()) / (RAND_MAX/2)) - 1;

    }
    
    translation_x += (translation_x < destination_x) ? TRANSLATION_STEP : -TRANSLATION_STEP;
    translation_y += (translation_y < destination_y) ? TRANSLATION_STEP : -TRANSLATION_STEP;
    
    assert(abs(translation_x) <= 1.0f);
    assert(abs(translation_y) <= 1.0f);
    assert(abs(destination_x) <= 1.0f);
    assert(abs(destination_y) <= 1.0f);
}

void update_position(){
    if(is_key_pressed){
        update_position_by_key();
    } else {
        update_position_randomly();
    }
}

int main(void)
{
    GLFWwindow* window;
    
    /* Initialize the library */
    if (!glfwInit())
        return -1;
    
    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }
    
    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    
    glfwSetKeyCallback(window, key_callback);
    
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        update_position();
        /* Render here */
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(232.0/255, 168.0/255, 124.0/255, 1.0f);
        
        glPushMatrix();
        glTranslated(translation_x, translation_y, 0);
        drawObject();
        glPopMatrix();
        
        /* Swap front and back buffers */
        glfwSwapBuffers(window);
        
        /* Poll for and process events */
        glfwPollEvents();
    }
    
    glfwTerminate();
    return 0;
}
